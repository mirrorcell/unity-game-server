﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ListItemController : MonoBehaviour
{
    public delegate void GameItemClicked(string gameId);
    public static event GameItemClicked OnGameItemClicked;

    public string GameId;
    public Text NumPlayers, Name;


    public void OnClick()
    {
        OnGameItemClicked(GameId);
    }
}