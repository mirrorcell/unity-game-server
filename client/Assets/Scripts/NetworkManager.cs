﻿using Phoenix;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkManager : MonoBehaviour
{
    public static NetworkManager inst = null;
    public static Socket socket;
    public static Channel gameChannel;
    public static bool ready = false;

    private void Awake()
    {
        if (inst == null) {
            inst = this;
        } else if (inst != this) {
            Destroy(gameObject);
        }

        string userId = PlayerPrefs.GetString("id");

        Debug.Log("Starting with " + userId);

        if (userId != "") {
            Connect(userId);
        }

        DontDestroyOnLoad(gameObject);
    }

    public static Socket Connect(string userId)
    {
        var server = ServerConnection.CurrentServer();

        var socketFactory = new BestHTTPWebsocketFactory();
        socket = new Socket(socketFactory);

        socket.OnOpen += onOpenCallback;
        socket.OnMessage += onMessageCallback;
        socket.OnError += onErrorCallback;
        socket.OnClose += onCloseCallback;

        Dictionary<string, string> socketParams = new Dictionary<string, string>();
        socketParams.Add("id", userId.ToString());

        socket.Connect(string.Format("{0}://{1}/socket", server.websocket_protocol, server.url), socketParams);

        return socket;
    }



    private static void Log(string type, string message)
    {
        Debug.Log(string.Format("[Socket] - {0}: {1}", type, message));
    }

    private static void onOpenCallback()
    {
        ready = true;
        Log("Open", "Socket Opened Succesfully");
    }

    private static void onMessageCallback(string m)
    {
        Log("Message", m);
    }

    private static void onErrorCallback(string m)
    {
        Log("Error", m);
    }

    private static void onCloseCallback(ushort code, string m)
    {
        Log("Closed", String.Format("Code: {0} \\ Message {1} ", code, m));
    }
}
