﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using Phoenix;
using UnityEngine.Networking.PlayerConnection;

[Serializable]
class User
{
    public string username;

    public User(string username)
    {
        this.username = username;
    }
}

public class UserForm : MonoBehaviour {

    public InputField UsernameField;
    private User MyUser;

    private void SetUsername(InputField input)
    {
        MyUser.username = input.text;
    }

    public void Start()
    {
        MyUser = new User("");
        UsernameField.onEndEdit.AddListener(delegate { SetUsername(UsernameField); });
    }

    public void OnSubmit()
    {
        StartCoroutine(CreateUser());
    }
    
    private IEnumerator CreateUser()
    {
        var Server = ServerConnection.CurrentServer();

        WWWForm form = new WWWForm();
        form.AddField("user", JsonUtility.ToJson(MyUser));

        using (UnityWebRequest www = UnityWebRequest.Post(String.Format("{0}://{1}/api/users", Server.http_protocol, Server.url), form)) {
            yield return www.Send();

            if (www.isNetworkError || www.isHttpError) {
                Debug.Log(www.error);
            } else {
                Debug.Log(www.downloadHandler.text);

                string text = www.downloadHandler.text;
                String[] arr =  text.Split(':');
                string idWithBrackets = arr[arr.Length - 1];
                string id = idWithBrackets.Substring(0, idWithBrackets.Length - 2);
                

                PlayerPrefs.SetString("username", MyUser.username);
                PlayerPrefs.SetString("id", id);

                NetworkManager.Connect(id);
            }
        }
    }
}
