﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game {
    public string Id;
    public string Name;
    public int NumPlayers;

    public Game(string id, string name, int numPlayers)
    {
        Id = id;
        Name = name;
        NumPlayers = numPlayers;
    }

}
