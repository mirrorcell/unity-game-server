﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyOrUser : MonoBehaviour {

    public GameObject LobbyView;
    public GameObject UserForm;

    private void Update()
    {
        if(PlayerPrefs.GetString("id") == "") {
            UserForm.SetActive(true);
            LobbyView.SetActive(false);
        } else {
            LobbyView.SetActive(true);
            UserForm.SetActive(false);
        }
    }
}
