﻿using Phoenix;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
    public void Awake()
    {
        if(NetworkManager.gameChannel == null) {
            if(NetworkManager.ready) {
                ConnectToGameChannel();
            } else {
                NetworkManager.socket.OnOpen += ConnectToGameChannel;
            }
        }
    }

    private void ConnectToGameChannel()
    {
        Debug.Log("Connecting To Game Channel");
        NetworkManager.gameChannel = NetworkManager.socket.MakeChannel(String.Format("game:{0}", PlayerPrefs.GetString("currentGameId")));

        NetworkManager.gameChannel.Join(null)
        .Receive(Reply.Status.Ok, r => OnJoinRoom(r))
        .Receive(Reply.Status.Error, r => Debug.Log("Join Error" + r.ToString()));
    }


    public void OnJoinRoom(Reply r)
    {
        Debug.Log("Joined Game");
    }

    public void LeaveGame()
    {
        Debug.Log("Leaving Game...");

        NetworkManager.gameChannel.Push("game_leave", null)
            .Receive(Reply.Status.Ok, r => OnLeftGame(r))
            .Receive(Reply.Status.Error, r => Debug.Log("ERROR"))
            .Receive(Reply.Status.Timeout, r => Debug.Log("TIMEOUT"));
    }

    public void OnLeftGame(Reply r)
    {
        SceneManager.LoadScene("Lobby");
        PlayerPrefs.DeleteKey("currentGameId");
    }
}
