﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerInfo {
    public string url;
    public string websocket_protocol = "ws";
    public string http_protocol = "http";

    public ServerInfo(string url, bool isSSL) {
        this.url = url;

        if(isSSL) {
            websocket_protocol = "wss";
            http_protocol = "https";
        }
    }
}

public class ServerConnection {
    public static ServerInfo LocalServer = new ServerInfo("127.0.0.1:4000", false);
    public static ServerInfo HerokuServer = new ServerInfo("stormy-scrubland-45938.herokuapp.com", true);

    public static ServerInfo CurrentServer()
    {
        return HerokuServer;
    }
}
