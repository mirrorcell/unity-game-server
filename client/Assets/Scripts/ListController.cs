﻿using Newtonsoft.Json.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ListController : MonoBehaviour {
    public GameObject ContentPanel;
    public GameObject ListItemPrefab;
    public GameObject LobbyObj;
    private LobbyManager Lm;
    private ArrayList listItems;

    public bool shouldUpdateGames = true;

    // Use this for initialization
    void Start () {
		if(LobbyObj != null) {
            Lm = LobbyObj.GetComponent<LobbyManager>();
        }

        listItems = new ArrayList();


	}
	
	// Update is called once per frame
	void Update () {
        if (shouldUpdateGames) {
            UpdateList(Lm.games);
            shouldUpdateGames = false;
        }
    }

    public void clearListItems()
    {
        foreach(GameObject item in listItems) {
            Destroy(item);
        }

        listItems.Clear();
    }
    
    public void UpdateList(ArrayList games)
    {
        Debug.Log("Updating List" + games.ToString());

        try {
            clearListItems();

            foreach (Game g in games) {
                GameObject newGameItem = Instantiate(ListItemPrefab) as GameObject;
                ListItemController c = newGameItem.GetComponent<ListItemController>();
                c.Name.text = g.Name;
                c.NumPlayers.text = g.NumPlayers.ToString();
                c.GameId = g.Id;

                newGameItem.transform.SetParent(ContentPanel.transform, false);
                newGameItem.transform.localScale = Vector3.one;

                listItems.Add(newGameItem);
            }
        } catch {
            Debug.LogError("Error updating list");
        }
    }

}
