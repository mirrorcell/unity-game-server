﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Phoenix;
using Newtonsoft.Json.Linq;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class LobbyManager : MonoBehaviour {
    private Channel channel;
    public ArrayList games;
    public GameObject listControllerObj;
    private ListController listController;
    public InputField gameNameInput;

    public void OnEnable()
    {
        ListItemController.OnGameItemClicked += OnGameClicked;
    }

    private void Start()
    {

        if(listControllerObj != null) {
            listController = listControllerObj.GetComponent<ListController>();
        }

        games = new ArrayList();

        if(NetworkManager.ready) {
            CreateChannel();
        } else {
            NetworkManager.socket.OnOpen += CreateChannel;
        }

    }

    private void CreateChannel()
    {
        channel = NetworkManager.socket.MakeChannel("lobby");
        channel.On(Message.InBoundEvent.phx_close, m => OnClose(m));
        channel.On("games_updated", m => OnReceiveGameUpdate(m));
        channel.On(Message.InBoundEvent.phx_error, m => OnClose(m));

        channel.Join(null)
            .Receive(Reply.Status.Ok, r => OnJoin(r))
            .Receive(Reply.Status.Error, r => Debug.Log("Join Error" + r.ToString()));
    }

    private void OnClose(Message m)
    {
        Debug.Log("Closed Lobby Channel" + m.ToString());
    }

    private void OnJoin(Reply reply)
    {
        Debug.Log("Joined Lobby Channel");

        channel.Push("get_current_games")
            .Receive(Reply.Status.Ok, r => OnReceiveGames(r));
    }

    public void CreateGame()
    {
        string name = gameNameInput.text;
        var payload = new Dictionary<string, object> {
                    { "name", name }
        };

        channel.Push("create_game", payload)
               .Receive(Reply.Status.Ok, r => Debug.Log("Created Game " + r.ToString()));
    }

    private void OnReceiveGameUpdate(Message m)
    {
        JArray games = m.payload["games"] as JArray;
        UpdateGames(games);
    }



    private void OnReceiveGames(Reply r)
    {
        JArray games = r.response["games"] as JArray;
        UpdateGames(games);
    }

    private void UpdateGames(JArray jsonGames)
    {
        try {
            games.Clear();

            foreach (JObject game in jsonGames) {
                Game thisGame = new Game((string)game["id"], (string)game["name"], (int)game["num_players"]);
                games.Add(thisGame);
            }

            listController.shouldUpdateGames = true;
        } catch {
            Debug.LogError("There was an error updating the games list");
        }
    }

    private void OnGameClicked(string gameId)
    {
        NetworkManager.gameChannel = NetworkManager.socket.MakeChannel(String.Format("game:{0}", gameId));

        NetworkManager.gameChannel.Join(null)
            .Receive(Reply.Status.Ok, r => OnJoinRoom(r, gameId))
            .Receive(Reply.Status.Error, r => Debug.Log("Join Error" + r.ToString()));

    }

    private void OnJoinRoom(Reply r, string gameId)
    {
        Debug.Log("Joined Room!!!!");

        PlayerPrefs.SetString("currentGameId", gameId);
        SceneManager.LoadScene("Game");
    }

    public void Logout()
    {
        PlayerPrefs.DeleteAll();
    }
}
